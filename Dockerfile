FROM php:8.0-fpm

RUN apt-get update && apt-get install -y  \
    git \
    zip \
    unzip \
    libmagickwand-dev \
    libonig-dev \
    libpq-dev \
    --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo_pgsql pgsql mbstring

COPY --from=composer /usr/bin/composer /usr/bin/composer
