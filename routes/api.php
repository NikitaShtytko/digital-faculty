<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EquipmentController;
use App\Http\Controllers\LaboratoryController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/auth')->group(function () {
    Route::post('/sign-up', [AuthController::class, 'signUp'])->name('authSignUp');
    Route::post('/sign-in', [AuthController::class, 'signIn'])->name('authSignIn');
});

Route::prefix('/recovery')->group(function () {
    Route::post('', [UserController::class, 'recover'])->name('userUpdate');
    Route::post('/email', [UserController::class, 'recoveryByEmail'])->name('currentUserInfo');
});

Route::prefix('/news')->group(function () {
    Route::get('', [NewsController::class, 'list'])->name('newsList');
    Route::get('/{id}', [NewsController::class, 'getById'])->name('newsById');
});

Route::prefix('/laboratories')->group(function () {
    Route::get('', [LaboratoryController::class, 'getLaboratoryList'])->name('getLaboratoryList');
    Route::get('/{id}', [LaboratoryController::class, 'getLaboratory'])->name('getLaboratory');
});

Route::prefix('/equipments')->group(function () {
    Route::get('', [EquipmentController::class, 'getEquipmentList'])->name('newEquipment');
    Route::get('/{id}', [EquipmentController::class, 'getEquipmentById'])->name('newEquipment');
});

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/users')->group(function () {
        Route::get('', [UserController::class, 'currentUser'])->name('currentUserInfo');
        Route::put('', [UserController::class, 'update'])->name('userUpdate');
        Route::delete('', [UserController::class, 'delete'])->name('userDelete');
        Route::post('/avatar', [UserController::class, 'newUserAvatar'])->name('userAvatar');
    });

    Route::prefix('/laboratory-requests')->group(function () {
        Route::post('', [LaboratoryController::class, 'makeRequest'])->name('makeRequest');
    });

    Route::middleware('admin')->group(function () {
        Route::prefix('/users')->group(function () {
            Route::get('/list', [UserController::class, 'list'])->name('usersList');
            Route::get('/{id}', [UserController::class, 'getById'])->name('userById');
        });

        Route::prefix('/news')->group(function () {
            Route::post('', [NewsController::class, 'newNews'])->name('newsNew');
            Route::put('/{id}', [NewsController::class, 'updateNews'])->name('newsUpdate');
            Route::delete('/{id}', [NewsController::class, 'delete'])->name('newsDelete');
        });

        Route::prefix('/laboratories')->group(function () {
            Route::post('', [LaboratoryController::class, 'newLaboratory'])->name('newLaboratory');
            Route::put('/{id}', [LaboratoryController::class, 'editLaboratory'])->name('editLaboratory');
            Route::delete('/{id}', [LaboratoryController::class, 'deleteLaboratory'])->name('deleteLaboratory');
            Route::post('/{id}/photos', [LaboratoryController::class, 'addLaboratoryPhotos'])->name('addLaboratoryPhoto');
            Route::delete('/{id}/photos/{photoId}', [LaboratoryController::class, 'deleteLaboratoryPhoto'])->name('deleteLaboratoryPhoto');
        });

        Route::prefix('/equipments')->group(function () {
            Route::post('', [EquipmentController::class, 'newEquipment'])->name('newEquipment');
            Route::put('/{id}', [EquipmentController::class, 'editEquipment'])->name('editEquipment');
            Route::delete('/{id}', [EquipmentController::class, 'deleteEquipment'])->name('deleteEquipment');
            Route::post('/{id}/photos', [EquipmentController::class, 'addEquipmentPhotos'])->name('editEquipment');
            Route::delete('/{id}/photos/{photoId}', [EquipmentController::class, 'deleteEquipmentPhoto'])->name('deleteEquipmentPhoto');
        });

        Route::prefix('/laboratory-requests')->group(function () {
            Route::get('', [LaboratoryController::class, 'listRequests'])->name('listRequests');
            Route::get('/{id}', [LaboratoryController::class, 'getRequest'])->name('getRequest');
            Route::put('/{id}', [LaboratoryController::class, 'requestDecision'])->name('requestDecision');
        });
    });
});


