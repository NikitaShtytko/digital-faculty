<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaboratoryCrudTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laboratories', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('contacts')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('laboratory_schedules', function (Blueprint $table) {
            $table->id();
            $table->timestamp('begin');
            $table->timestamp('end');
            $table->foreignId('laboratory_id');
            $table->foreign('laboratory_id')->references('id')->on('laboratories')
            ->cascadeOnUpdate()->cascadeOnDelete();
        });

        Schema::create('laboratory_logs', function (Blueprint $table) {
            $table->id();
            $table->string('action');
            $table->timestamps();
            $table->foreignId('laboratory_id');
            $table->foreign('laboratory_id')->references('id')->on('laboratories')
            ->cascadeOnUpdate()->cascadeOnDelete();
        });

        Schema::create('laboratory_files', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('path');
            $table->bigInteger('size');
            $table->foreignId('laboratory_id');
            $table->timestamps();
            $table->foreign('laboratory_id')->references('id')->on('laboratories')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });

        Schema::create('equipments', function (Blueprint $table) {
            $table->id();
            $table->string('description')->nullable();
            $table->foreignId('laboratory_id');
            $table->timestamps();
            $table->foreign('laboratory_id')->references('id')->on('laboratories')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });

        Schema::create('equipment_files', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('path');
            $table->bigInteger('size');
            $table->foreignId('equipment_id');
            $table->timestamps();
            $table->foreign('equipment_id')->references('id')->on('equipments')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });

        Schema::create('laboratory_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('laboratory_id');
            $table->foreignId('equipment_id');
            $table->foreignId('user_id');
            $table->string('description')->nullable();
            $table->enum('status', ['opened', 'approved', 'rejected'])->default('opened');
            $table->timestamps();
            $table->foreign('laboratory_id')->references('id')->on('laboratories');
            $table->foreign('equipment_id')->references('id')->on('equipments');
            $table->foreign('user_id')->references('id')->on('users')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratory_crud_tables');
    }
}
