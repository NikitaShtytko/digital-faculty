<?php

namespace Database\Factories;

use App\Models\LaboratoryRequest;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class LaboratoryRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LaboratoryRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->text(50),
            'status' => LaboratoryRequest::STATUS_OPENED,
            'date' => (new \DateTime())->modify('+5 day')->format('Y-m-d H:i:s'),
        ];
    }
}
