<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'certificate' => $this->faker->numberBetween(100000, 999999),
            'password' => Hash::make('123qwe123'),
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
        ];
    }
}
