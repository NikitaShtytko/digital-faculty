<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class UserWithProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $this */
        return [
            'id' => $this->id,
            'email' => $this->email,
            'phone' => $this->phone,
            'avatar' => $this->avatar?->url,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'parent_name' => $this->parent_name,
            'sex' => $this->sex,
            'chair' => $this->chair,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
