<?php

namespace App\Http\Resources;

use App\Models\Equipment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class EquipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Equipment $this */
        return [
            'id' => $this->id,
            'laboratory' => new LaboratoryResource($this->whenLoaded('laboratory')),
            'description' => $this->description,
            'photos' => $this->photos,
        ];
    }
}
