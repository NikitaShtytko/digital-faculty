<?php

namespace App\Http\Resources;

use App\Models\Equipment;
use App\Models\Laboratory;
use App\Models\LaboratorySchedule;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class LaboratoryWithSchedulesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Laboratory $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'contacts' => $this->contacts,
            'schedules' => LaboratorySchedulesResource::collection(
                $this->schedules
            )
        ];
    }
}
