<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserInfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'nullable|string',
            'last_name' => 'nullable|string',
            'parent_name' => 'nullable|string',
            'certificate' => 'nullable|string',
            'phone' => 'nullable|string|unique:users,phone',
            'email' => 'nullable|string|unique:users,email',
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
        ];
    }
}
