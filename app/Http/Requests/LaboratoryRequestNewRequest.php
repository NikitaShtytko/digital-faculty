<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LaboratoryRequestNewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'laboratory_id' => 'required|int|exists:laboratories,id',
            'equipment_id' => [
                'required',
                Rule::exists('equipments', 'id')
                    ->where('id', $this->input('equipment_id'))
                    ->where('laboratory_id', $this->input('laboratory_id')),
            ],
            'description' => 'required|string',
            'date' => 'required|date_format:Y-m-d|after:now'
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
        ];
    }
}
