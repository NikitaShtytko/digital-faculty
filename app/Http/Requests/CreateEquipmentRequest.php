<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEquipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'laboratory_id' => 'required|integer|exists:laboratories,id',
            'description' => 'nullable|string',
            'files' => 'nullable|array',
            'files.*' => 'required|file',
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
        ];
    }
}
