<?php

namespace App\Http\Controllers;

use App\Http\Requests\EndRecoveryRequest;
use App\Http\Requests\FileUploadRequest;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\StartRecoveryRequest;
use App\Http\Requests\UserInfoUpdateRequest;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserWithProfileResource;
use App\Models\UserFile;
use App\Services\NewsService;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserService
     */
    public $userService;

    /**
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->userService = $service;
    }


    /**
     * @return UserWithProfileResource
     */
    public function newUserAvatar(FileUploadRequest $request)
    {
        /** @var UserFile $file */
        $file = $this->userService->newUserAvatar($request->file('file'));

        return new UserWithProfileResource($file->user);
    }

    /**
     * @return UserWithProfileResource
     */
    public function currentUser()
    {
        return new UserWithProfileResource(
            $this->userService->getById(Auth::id())
        );
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function list()
    {
        return UserWithProfileResource::collection(
            $this->userService->getUsersList()
        );
    }

    /**
     * @return UserWithProfileResource
     */
    public function getById(int $id)
    {
        return new UserWithProfileResource(
            $this->userService->getById($id)
        );
    }

    /**
     * @return void
     */
    public function update(UserInfoUpdateRequest $request)
    {
        $this->userService->update($request->all());
    }

    /**
     * @return void
     */
    public function delete()
    {
        $this->userService->deleteUser();
    }

    public function recoveryByEmail(StartRecoveryRequest $request)
    {
        $this->userService->recoveryByEmail($request->all());
    }

    public function recover(EndRecoveryRequest $request)
    {
        return $this->userService->recover($request->all());
    }
}
