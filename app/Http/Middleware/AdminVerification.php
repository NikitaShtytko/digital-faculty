<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class AdminVerification extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        if (Auth::user() && Auth::user()->isAdmin()) {
            return $next($request);
        }

        return abort(403);
    }
}
