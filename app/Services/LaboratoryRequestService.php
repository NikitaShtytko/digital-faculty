<?php

namespace App\Services;

use App\Http\Requests\LaboratoryRequestNewRequest;
use App\Models\LaboratoryRequest;
use App\Repositories\LaboratoryRepository;
use App\Repositories\LaboratoryRequestRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class LaboratoryRequestService
{
    /**
     * @var LaboratoryRequestRepository
     */
    private $laboratoryRequestRepository;

    /**
     */
    public function __construct()
    {
        $this->laboratoryRequestRepository = new LaboratoryRequestRepository();
    }

    /**
     * @return Builder[]|Collection
     */
    public function list($data)
    {
        if (isset($data['laboratory_id']))
            $this->laboratoryRequestRepository->setLaboratoryId($data['laboratory_id']);

        return $this->laboratoryRequestRepository->requestsList();
    }

    /**
     * @param int $id
     * @return array
     */
    public function getById(int $id)
    {
        /** @var LaboratoryRequest $request */
        $request = $this->laboratoryRequestRepository->getById($id);

        $laboratoryRepository = new LaboratoryRepository();
        $laboratory = $laboratoryRepository->getLaboratoryWithSchedules($request->laboratory_id);

        return [
            'request' => $request,
            'laboratory' => $laboratory,
        ];
    }

    /**
     * @param int $requestId
     * @param array $data
     */
    public function makeDecision(int $requestId, array $data)
    {
        $request = $this->laboratoryRequestRepository->update($requestId, $data);
        if ($data['decision'] === LaboratoryRequest::STATUS_APPROVED)
            (new LaboratoryRepository())->createScheduleFromRequest($request);
    }

    /**
     * @param array $data
     * @return LaboratoryRequest
     */
    public function newRequest(array $data)
    {
        return $this->laboratoryRequestRepository->newRequest($data);
    }

}
