<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Mail;

class EmailService
{
    /**
     * Send an e-mail reminder to the user.
     *
     * @param int $id
     * @return void
     */
    public function sendRecoveryEmail($id)
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        Mail::send('recovery', ['email' => $user->email, 'code' => $user->code], function ($m) use ($user) {
            $m->from('edurfe@bsu.by');

            $m->to($user->email)->subject('Восстановление доступа');
        });
    }
}
