<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Str;
use App\Repositories\ProfileRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @var UserRepository
     */
    public $userRepository;

    public function __construct(UserRepository $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * @param array $data
     * @return Builder|Model|User
     */
    public function loginUser(array $data)
    {
        /** @var User $user */
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = $this->userRepository->findUserByEmail($data['email']);
            $user->token = $user->createToken('mobile')->plainTextToken;
            return $user;
        }

        abort(401, 'Wrong credentials');
    }

    /**
     * @param array $data
     * @return \App\Models\User
     * @throws \Throwable
     */
    public function registerNewUser(array $data): User
    {
        $profileRepository = new ProfileRepository();

        $user = $this->userRepository->createNewUser($data);
        $user->token = $user->createToken('mobile')->plainTextToken;

        $data['user_id'] = $user->id;
        $profileRepository->newProfile($data);

        return $user;
    }

    /**
     * @param $file
     * @return mixed
     */
    public function newUserAvatar($file)
    {
        $fileService = new FileService();

        return $fileService->changeUserAvatar($file);
    }

    /**
     * @return Builder[]|Collection
     */
    public function getUsersList()
    {
        return $this->userRepository->getUsersList();
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function getById(int $id)
    {
        return $this->userRepository->getUserById($id);
    }

    /**
     * @param array $data
     */
    public function update(array $data)
    {
        $this->userRepository->update($data);

        $profileRepository = new ProfileRepository();
        $profileRepository->update($data);
    }

    /**
     *
     */
    public function deleteUser()
    {
        $fileRepository = new FileService();
        $fileRepository->deleteUserFile(Auth::id());

        $this->userRepository->deleteUserById(Auth::id());
    }

    public function recoveryByEmail(array $data)
    {
        /** @var User $user */
        try {
            $user = $this->userRepository->findUserByEmail($data['email']);
            $user->code = rand(100000, 999999);
            $user->save();

            (new EmailService())->sendRecoveryEmail($user->id);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function recover(array $data)
    {
        /** @var User $user */
        $user = $this->userRepository->findUserByEmail($data['email']);

        if ($user->code === $data['code']){
            $newPassword = Str::random(9);
            $user->password = Hash::make($newPassword);
            $user->code = null;
            $user->save();

            return "Новый пароль:  \". $newPassword\"";
        }

        return null;
    }
}
