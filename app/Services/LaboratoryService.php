<?php

namespace App\Services;

use App\Repositories\LaboratoryRepository;

class LaboratoryService
{
    /**
     * @var LaboratoryRepository
     */
    public $laboratoryRepository;

    /**
     * @param LaboratoryRepository $service
     */
    public function __construct(LaboratoryRepository $service)
    {
        $this->laboratoryRepository = $service;
    }

    public function createLaboratory(array $all, ?array $files)
    {
        $laboratory = $this->laboratoryRepository->createLaboratory($all);
        $laboratory?->load('equipments');

        if (!empty($files)) {
            $fileService = new FileService();
            $fileService->setOwnerId($laboratory->id);
            foreach ($files as $file) {
                $fileService->uploadLaboratoryFile($file);
            }
        }

        return $laboratory;
    }

    public function getLaboratory(int $laboratoryId)
    {
        return $this->laboratoryRepository->getLaboratoryById($laboratoryId);
    }

    public function getLaboratoryList()
    {
        return $this->laboratoryRepository->getLaboratoryList();
    }

    public function deleteLaboratory(int $laboratoryId)
    {
        (new FileService())->deleteLaboratoryFiles($laboratoryId);
        $this->laboratoryRepository->deleteById($laboratoryId);
    }

    public function addLaboratoryPhotos(int $laboratoryId, ?array $files)
    {
        if (empty($files))
            return;

        if (!$this->getLaboratory($laboratoryId))
            return null;

        $fileService = new FileService();
        $fileService->setOwnerId($laboratoryId);
        foreach ($files as $file) {
            $fileService->uploadLaboratoryFile($file);
        }
    }

    public function deleteLaboratoryPhoto(int $laboratoryId, int $photoId)
    {
        $fileService = new FileService();
        $fileService->setOwnerId($laboratoryId);
        $fileService->deleteLaboratoryFile($photoId);
    }

    public function editLaboratory(int $laboratoryId, array $all)
    {
        $laboratory = $this->laboratoryRepository->editLaboratory($laboratoryId, $all);
        $laboratory?->load('equipments');

        return $laboratory;
    }
}
