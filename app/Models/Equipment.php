<?php

namespace App\Models;

use App\Models\Files\EquipmentFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property string $description
 * @property integer $laboratory_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Laboratory $laboratory
 * @property EquipmentFile[] $photos
 *
 *  * Class News
 * @package App\Models
 */
class Equipment extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'equipments';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'description',
    ];

    /**
     * @return BelongsTo
     */
    public function laboratory()
    {
        return $this->belongsTo(Laboratory::class, 'laboratory_id');
    }

    /**
     * @return HasMany
     */
    public function photos()
    {
        return $this->hasMany(EquipmentFile::class, 'equipment_id',);
    }
}
