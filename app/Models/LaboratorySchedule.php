<?php

namespace App\Models;

use App\Models\Files\LaboratoryFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property integer $laboratory_id
 * @property integer $equipment_id
 * @property integer $user_id
 * @property string $description
 * @property string $begin
 * @property string $end
 *
 * @property User $user
 * @property Equipment $equipment
 * @property Laboratory $laboratory
 *
 *  * Class News
 * @package App\Models
 */
class LaboratorySchedule extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'description',
    ];

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function equipment()
    {
        return $this->belongsTo(Equipment::class, 'equipment_id');
    }

    /**
     * @return BelongsTo
     */
    public function laboratory()
    {
        return $this->belongsTo(Laboratory::class, 'laboratory_id');
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeExactLaboratory($laboratoryId)
    {
        return $this->where('laboratory_id', '=', $laboratoryId);
    }
}
