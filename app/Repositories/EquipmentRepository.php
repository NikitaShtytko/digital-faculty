<?php

namespace App\Repositories;

use App\Models\Equipment;

class EquipmentRepository
{
    public function createEquipment(array $all)
    {
        $equipmentModel = new Equipment();
        $equipmentModel->laboratory_id = $all['laboratory_id'];
        $equipmentModel->fill($all)->save();

        return $equipmentModel;
    }

    /**
     * @param int $equipmentId
     * @param array $all
     * @return Equipment
     */
    public function editEquipment(int $equipmentId, array $all)
    {
        /** @var Equipment $equipmentModel */
        $equipmentModel = Equipment::query()
            ->where('id', '=', $equipmentId)
            ->first();

        if (!$equipmentModel) return null;

        if (isset($all['laboratory_id']))
            $equipmentModel->laboratory_id = $all['laboratory_id'];

        $equipmentModel->fill($all)->save();

        return $equipmentModel->refresh();
    }

    /**
     * @param int $equipmentId
     * @return void
     */
    public function deleteEquipment(int $equipmentId)
    {
        Equipment::query()
            ->where('id', '=', $equipmentId)
            ->delete();
    }

    /**
     * @param int $equipmentId
     * @return mixed
     */
    public function findById(int $equipmentId)
    {
        return Equipment::query()
            ->with('laboratory')
            ->findOrFail($equipmentId);
    }

    /**
     * @return mixed
     */
    public function findAll()
    {
        return Equipment::query()
            ->with('laboratory')
            ->get();
    }
}
