<?php

namespace App\Repositories;

use App\Models\Laboratory;
use App\Models\LaboratoryRequest;
use App\Models\LaboratorySchedule;

class LaboratoryRepository
{
    public function createLaboratory(array $all)
    {
        $laboratoryModel = new Laboratory();
        $laboratoryModel->fill($all)->save();

        return $laboratoryModel;
    }

    public function getLaboratoryById(int $id)
    {
        return Laboratory::query()
            ->with('equipments')
            ->findOrFail($id);
    }

    public function getLaboratoryWithSchedules(int $id)
    {
        return Laboratory::query()
            ->with('schedules')
            ->findOrFail($id);
    }

    public function getLaboratoryList()
    {
        return Laboratory::query()
            ->with('equipments')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function deleteById(int $laboratoryId)
    {
        Laboratory::query()
            ->where('id', '=', $laboratoryId)
            ->delete();
    }

    public function editLaboratory(int $laboratoryId, array $all)
    {
        /** @var Laboratory $equipmentModel */
        $laboratoryModel = Laboratory::query()
            ->where('id', '=', $laboratoryId)
            ->first();

        if (!$laboratoryModel) return null;

        $laboratoryModel->fill($all)->save();

        return $laboratoryModel->refresh();
    }

    /**
     * @param LaboratoryRequest $request
     * @return void
     */
    public function createScheduleFromRequest(LaboratoryRequest $request)
    {
        $laboratorySchedule = new LaboratorySchedule();

        $laboratorySchedule->fill($request->toArray());
        $laboratorySchedule->user_id = $request->user_id;
        $laboratorySchedule->laboratory_id = $request->laboratory_id;
        $laboratorySchedule->equipment_id = $request->equipment_id;

        $laboratorySchedule->begin = $request->date;
        $laboratorySchedule->end = $request->date;

        $laboratorySchedule->save();
    }

    public function getLaboratorySchedules(int $laboratory_id)
    {
        return LaboratorySchedule::query()
            ->where('laboratory_id', $laboratory_id)
            ->get();
    }
}
