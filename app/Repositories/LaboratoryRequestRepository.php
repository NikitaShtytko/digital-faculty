<?php

namespace App\Repositories;

use App\Models\LaboratoryRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LaboratoryRequestRepository
{
    private $laboratoryId;

    public function __construct(){}

    /**
     * @param mixed $laboratoryId
     */
    public function setLaboratoryId($laboratoryId): void
    {
        $this->laboratoryId = $laboratoryId;
    }

    /**
     * @return Builder[]|Collection
     */
    public function requestsList()
    {
        $laboratoryRequestObject = new LaboratoryRequest();
        $query = $laboratoryRequestObject::query()->orderBy('id');

        if ($this->laboratoryId){
            $query = $laboratoryRequestObject->scopeExactLaboratory($this->laboratoryId);
        }

        return $query->get();
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function getById(int $id)
    {
        return LaboratoryRequest::query()
            ->findOrFail($id);
    }

    /**
     * @param int $requestId
     * @param array $data
     * @return LaboratoryRequest
     */
    public function update(int $requestId, array $data)
    {
        /** @var LaboratoryRequest $request */
        $request = LaboratoryRequest::query()
            ->findOrFail($requestId);

        $request->status = $data['decision'];
        $request->save();

        return $request->refresh();
    }

    /**
     * @param array $data
     * @return LaboratoryRequest
     */
    public function newRequest(array $data)
    {
        $request = new LaboratoryRequest();
        $request->user_id = Auth::id();
        $request->fill($data)->save();

        return $request;
    }
}
