<?php

namespace App\Repositories;

use App\Models\News;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class NewsRepository
{
    /**
     * @return Builder[]|Collection
     */
    public function getNewsList()
    {
        return News::query()
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function getNewsById(int $id)
    {
        return News::query()
            ->findOrFail($id);
    }

    /**
     * @param array $data
     * @return News
     */
    public function createNews(array $data)
    {
        $news = new News();
        $news->fill($data);
        $news->user_id = Auth::id();
        $news->save();

        return $news;
    }

    public function delete(int $newsId)
    {
        return News::query()
            ->where('id', $newsId)
            ->delete();
    }
}
