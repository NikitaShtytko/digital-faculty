<?php

namespace App\Repositories;

use App\Models\Files\EquipmentFile;
use App\Models\Files\LaboratoryFile;
use App\Models\Files\NewsFile;
use App\Models\UserFile;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class FileRepository
{
    const USER_FILE = 'USER_FILE';
    const NEWS_FILE = 'NEWS_FILE';
    const LABORATORY_FILE = 'LABORATORY_FILE';
    const EQUIPMENT_FILE = 'EQUIPMENT_FILE';


    /**
     * @return Builder|Model
     */
    public function findUserFile(int $userId)
    {
        return UserFile::query()
            ->where('user_id', $userId)
            ->first();
    }

    /**
     * @return Builder[]|Collection
     */
    public function findNewsFiles(int $newsId)
    {
        return NewsFile::query()
            ->where('news_id', $newsId)
            ->get();
    }

    public function findLaboratoryFiles($laboratoryId)
    {
        return LaboratoryFile::query()
            ->where('laboratory_id', $laboratoryId)
            ->get();
    }


    public function findLaboratoryFile($laboratoryId, $fileId)
    {
        return LaboratoryFile::query()
            ->where('id', $fileId)
            ->where('laboratory_id', $laboratoryId)
            ->firstOrFail();
    }


    public function findEquipmentFiles(int $equipmentId)
    {
        return EquipmentFile::query()
            ->where('equipment_id', $equipmentId)
            ->get();
    }

    public function findEquipmentFile($equipmentId, $fileId)
    {
        return EquipmentFile::query()
            ->where('id', '=', $fileId)
            ->where('equipment_id', '=', $equipmentId)
            ->first();
    }

    /**
     * @return Builder|Model
     */
    public function uploadUserFile($file, int $userId)
    {
        return UserFile::query()
            ->create([
                'user_id' => $userId,
                'name' => $file->getClientOriginalName(),
                'path' => $file->path,
                'size' => $file->getSize()
            ]);
    }

    /**
     * @return Builder|Model
     */
    public function uploadNewsFile($file)
    {
        return NewsFile::query()
            ->create([
                'news_id' => $file->origin,
                'name' => $file->getClientOriginalName(),
                'path' => $file->path,
                'size' => $file->getSize()
            ]);
    }

    public function uploadLaboratoryFile($file)
    {
        return LaboratoryFile::query()
            ->create([
                'laboratory_id' => $file->origin,
                'name' => $file->getClientOriginalName(),
                'path' => $file->path,
                'size' => $file->getSize()
            ]);
    }

    public function uploadEquipmentFile($file)
    {
        return EquipmentFile::query()
            ->create([
                'equipment_id' => $file->origin,
                'name' => $file->getClientOriginalName(),
                'path' => $file->path,
                'size' => $file->getSize()
            ]);
    }

    public function deleteFileRecord($table, array $fileIds)
    {
        switch ($table) {
            case self::USER_FILE:
                $query = UserFile::query();
                break;
            case self::NEWS_FILE:
                $query = NewsFile::query();
                break;
            case self::LABORATORY_FILE:
                $query = LaboratoryFile::query();
                break;
            case self::EQUIPMENT_FILE:
                $query = EquipmentFile::query();
                break;
            default:
                return;
        }

        $query->whereIn('id', $fileIds)
            ->delete();
    }
}
