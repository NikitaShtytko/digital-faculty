<?php

namespace Tests\Feature;

use App\Models\News;
use App\Models\User;
use Faker\Factory;
use Faker\Generator;

class NewsTest extends \Tests\TestCase
{
    protected Generator $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    protected function createUser($attributes = [])
    {
        $user = User::factory()->state($attributes)->create();
        $accessToken = $user->createToken('test')->plainTextToken;

        $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ]);

        return $user;
    }

    protected function createNews($attributes = [])
    {
        return News::factory()->state($attributes)->create();
    }

    public function testNewsList()
    {
        /** @var User $user */
        $user = $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var News $news */
        $news = $this->createNews(['user_id' => $user->id]);

        $response = $this->get('/api/news')
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertEquals($news->id, $response['data'][0]['id']);
        $this->assertEquals($news->title, $response['data'][0]['title']);
        $this->assertEquals($news->description, $response['data'][0]['description']);
        $this->assertEquals($news->user->email, $response['data'][0]['user']['email']);
    }


    public function testNewsById()
    {
        /** @var User $user */
        $user = $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var News $news */
        $news = $this->createNews(['user_id' => $user->id]);

        $response = $this->get('/api/news/' . $news->id)
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertEquals($news->id, $response['data']['id']);
        $this->assertEquals($news->title, $response['data']['title']);
        $this->assertEquals($news->description, $response['data']['description']);
        $this->assertEquals($news->user->email, $response['data']['user']['email']);
    }

    public function testNewsByWrongId()
    {
        $this->get('/api/news/0')->assertNotFound();
    }

    public function testCreateNews()
    {
        /** @var User $user */
        $user = $this->createUser(['role' => User::ROLE_ADMIN]);
        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->name,
        ];

        $response = $this->post('/api/news', $data)
            ->assertCreated();

        $response = $response->decodeResponseJson();
        $this->assertEquals($response['data']['title'], $data['title']);
        $this->assertEquals($response['data']['description'], $data['description']);
        $this->assertEquals($response['data']['user']['email'], $user->email);
        $this->assertEquals($response['data']['user']['phone'], $user->phone);
    }

    public function testCreateNewsByUser()
    {
        $this->createUser();

        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->name,
        ];

        $this->post('/api/news', $data)
            ->assertForbidden();
    }

    public function testUpdateNews()
    {
        /** @var User $user */
        $user = $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var News $news */
        $news = $this->createNews(['user_id' => $user->id]);

        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->name,
        ];

        $response = $this->put('/api/news/' . $news->id, $data)
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertEquals($news->id, $response['data']['id']);
        $this->assertEquals($news->user->email, $response['data']['user']['email']);

        $this->assertEquals($data['title'], $response['data']['title']);
        $this->assertEquals($data['description'], $response['data']['description']);

        $this->assertNotEquals($news->title, $response['data']['title']);
        $this->assertNotEquals($news->description, $response['data']['description']);
    }

    public function testUpdateNewsByUser()
    {
        /** @var User $user */
        $user = $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var News $news */
        $news = $this->createNews(['user_id' => $user->id]);

        $this->createUser();
        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->name,
        ];

        $this->put('/api/news/' . $news->id, $data)
            ->assertForbidden();
    }

    public function testDeleteNews()
    {
        /** @var User $user */
        $user = $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var News $news */
        $news = $this->createNews(['user_id' => $user->id]);

        $this->delete('/api/news/' . $news->id)
            ->assertOk();

        $this->assertNull(News::query()->find($news->id));
    }

    public function testDeleteNewsWrongId()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);
        $this->delete('/api/news/0')->assertOk();
    }

    public function testDeleteNewsByUser()
    {
        $this->createUser();
        $this->delete('/api/news/0')->assertForbidden();
    }
}
