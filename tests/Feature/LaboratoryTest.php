<?php

namespace Tests\Feature;

use App\Models\Equipment;
use App\Models\Laboratory;
use App\Models\Profile;
use App\Models\User;
use Faker\Factory;
use Faker\Generator;

class LaboratoryTest extends \Tests\TestCase
{
    protected Generator $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    protected function createUser($attributes = [])
    {
        /** @var User $user */
        $user = User::factory()->state($attributes)->create();
        $accessToken = $user->createToken('test')->plainTextToken;

        Profile::factory()->state(['user_id' => $user->id])->create();

        $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ]);

        return $user;
    }

    protected function createLaboratory($attributes = [])
    {
        /** @var Laboratory $user */
        return Laboratory::factory()->state($attributes)->create();
    }

    public function testCreateLaboratory()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);
        $data = [
            'name' => $this->faker->name,
            'contacts' => $this->faker->phoneNumber,
            'description' => $this->faker->text(50),
        ];

        $response = $this
            ->post('/api/laboratories', $data)
            ->assertJsonFragment([
                'name' => $data['name'],
                'contacts' => $data['contacts'],
                'description' => $data['description'],
                'photos' => [],
                'equipments' => [],
            ]);

        $response->assertCreated();
    }

    public function testCreateLaboratoryByUser()
    {
        $this->createUser();
        $data = [
            'name' => $this->faker->name,
            'contacts' => $this->faker->phoneNumber,
            'description' => $this->faker->text(50),
        ];

        $this
            ->post('/api/laboratories', $data)
            ->assertForbidden();
    }

    public function testUpdateLaboratory()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();

        $data = [
            'name' => $this->faker->name,
            'contacts' => $this->faker->phoneNumber,
            'description' => $this->faker->text(50),
        ];

        $response = $this
            ->put("/api/laboratories/$laboratory->id", $data)
            ->assertJsonFragment([
                'name' => $data['name'],
                'contacts' => $data['contacts'],
                'description' => $data['description'],
                'photos' => [],
                'equipments' => [],
            ]);

        /** @var Laboratory $updatedLaboratory */
        $updatedLaboratory = Laboratory::query()->find($laboratory->id);
        $this->assertNotEquals($updatedLaboratory->name, $laboratory->name);
        $this->assertNotEquals($updatedLaboratory->contacts, $laboratory->contacts);
        $this->assertNotEquals($updatedLaboratory->description, $laboratory->description);

        $response->assertOk();
    }

    public function testUpdateLaboratoryByUser()
    {
        $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        $data = [
            'name' => $this->faker->name,
            'contacts' => $this->faker->phoneNumber,
            'description' => $this->faker->text(50),
        ];

        $this
            ->put("/api/laboratories/$laboratory->id", $data)
            ->assertForbidden();
    }

    public function testUpdateLaboratoryByWrongId()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);

        $response = $this
            ->put("/api/laboratories/0", [])
            ->assertNotFound();
    }

    public function testLaboratoryList()
    {
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();

        $response = $this
            ->get("/api/laboratories")
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertEquals($response['data'][0]['name'], $laboratory->name);
        $this->assertEquals($response['data'][0]['contacts'], $laboratory->contacts);
        $this->assertEquals($response['data'][0]['description'], $laboratory->description);
    }

    public function testLaboratoryById()
    {
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();

        $response = $this
            ->get("/api/laboratories/$laboratory->id")
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertEquals($response['data']['name'], $laboratory->name);
        $this->assertEquals($response['data']['contacts'], $laboratory->contacts);
        $this->assertEquals($response['data']['description'], $laboratory->description);
    }
}
