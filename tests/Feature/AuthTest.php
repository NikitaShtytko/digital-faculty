<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Factory;
use Faker\Generator;

class AuthTest extends \Tests\TestCase
{
    protected Generator $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    protected function createUser($attributes = [])
    {
        $user = User::factory()->state($attributes)->create();
        $accessToken = $user->createToken('test')->plainTextToken;

        $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ]);

        return $user;
    }

    public function testSignUp()
    {
        $data = [
            'fistName' => $this->faker->firstName,
            'lastName' => $this->faker->lastName,
            'certificate' => $this->faker->numberBetween(100000, 999999),
            'password' => '123qwe123',
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'sex' => 'male',
        ];

        $response = $this
            ->post('/api/auth/sign-up', $data)
            ->assertJsonFragment([
                'email' => $data['email'],
                'phone' => $data['phone'],
                'is_email_verify' => false,
            ]);

        $this->assertArrayHasKey('token', $response->original);

        $response->assertCreated();
    }

    public function testSignIn()
    {
        $user = $this->createUser();

        $response = $this
            ->post('/api/auth/sign-in', [
                'email' => $user['email'],
                'password' => '123qwe123'
            ])
            ->assertJsonFragment([
                'email' => $user['email'],
                'phone' => $user['phone'],
            ]);

        $this->assertArrayHasKey('token', $response->original);

        $response->assertOk();
    }

    public function testSignInFailure()
    {
        $user = $this->createUser();

        $response = $this
            ->post('/api/auth/sign-in', [
                'email' => $user['email'],
                'password' => $this->faker->password
            ])
            ->assertJsonFragment([
                'message' => 'Wrong credentials',
            ])
            ->assertUnauthorized();
    }
}
