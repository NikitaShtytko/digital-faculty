<?php

namespace Tests\Feature;

use App\Models\Profile;
use App\Models\User;
use Faker\Factory;
use Faker\Generator;

class UserTest extends \Tests\TestCase
{
    protected Generator $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    protected function createUser($attributes = [])
    {
        /** @var User $user */
        $user = User::factory()->state($attributes)->create();
        $accessToken = $user->createToken('test')->plainTextToken;

        Profile::factory()->state(['user_id' => $user->id])->create();

        $this->withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ]);

        return $user;
    }

    public function testCurrentUser()
    {
        $user = $this->createUser();

        $response = $this
            ->get('/api/users')
            ->assertJsonFragment([
                'email' => $user['email'],
                'phone' => $user['phone'],
                'first_name' => $user->profile->first_name,
                'last_name' => $user->profile->last_name,
                'sex' => $user->profile->sex,
            ]);

        $response->assertOk();
    }

    public function testUsersList()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);

        $response = $this
            ->get('/api/users/list')
            ->assertOk();

        $response = $response->decodeResponseJson();
        foreach ($response['data'] as $user){
            $this->assertArrayHasKey('id', $user);
            $this->assertArrayHasKey('first_name', $user);
            $this->assertArrayHasKey('last_name', $user);
            $this->assertArrayHasKey('email', $user);
            $this->assertArrayHasKey('phone', $user);
        }
    }

    public function testUsersListForbidden()
    {
        $this->createUser();

        $this
            ->get('/api/users/list')
            ->assertForbidden();
    }

    public function testUserById()
    {
        $user = $this->createUser();
        $this->createUser(['role' => User::ROLE_ADMIN]);

        $this->get('/api/users/' . $user->id)
            ->assertJsonFragment([
                'id' => $user->id,
                'first_name' => $user->profile->first_name,
                'last_name' => $user->profile->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
            ])->assertOk();
    }

    public function testUserByIdForbidden()
    {
        $user = $this->createUser();
        $this->createUser();

        $this->get('/api/users/' . $user->id)
            ->assertForbidden();
    }

    public function testUpdateProfile()
    {
        $user = $this->createUser();
        $profile = $user->profile;

        $data = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
        ];

         $this->put('/api/users', $data)
            ->assertOk();

        /** @var User $updatedUser */
        $updatedUser = User::query()->find($user->id);
        $this->assertNotEquals($profile->first_name, $updatedUser->profile->first_name);
        $this->assertNotEquals($profile->last_name, $updatedUser->profile->last_name);
        $this->assertNotEquals($user->phone, $updatedUser->phone);
        $this->assertNotEquals($user->email, $updatedUser->email);
    }

    public function testUserDelete()
    {
        $user = $this->createUser();

        $this->delete('/api/users')
            ->assertOk();

        $this->assertNull(User::query()->find($user->id));
    }
}
