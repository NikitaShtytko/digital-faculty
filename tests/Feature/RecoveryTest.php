<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Factory;
use Faker\Generator;

class RecoveryTest extends \Tests\TestCase
{
    protected Generator $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    protected function createUser($attributes = [])
    {
        return User::factory()->state($attributes)->create();
    }

    public function testStartRecovery()
    {
        /** @var User $user */
        $user = $this->createUser();

        $this->post('/api/recovery/email', ['email' => $user->email])
            ->assertOk();
    }

    public function testEndRecovery()
    {
        /** @var User $user */
        $user = $this->createUser(['code' => \Illuminate\Support\Str::random(9)]);
        /** @var User $data */
        $data = User::query()->find($user->id);

        $this->post('/api/recovery', [
                'email' => $user['email'],
                'code' => $user['code']
            ])->assertOk();

        $user->refresh();
        $this->assertNotEquals($user->password, $data->password);
        $this->assertNull($user->code);
    }

    public function testEndRecoveryWrongCode()
    {
        /** @var User $user */
        $user = $this->createUser(['code' => \Illuminate\Support\Str::random(9)]);
        /** @var User $data */
        $data = User::query()->find($user->id);

        $this->post('/api/recovery', [
            'email' => $user['email'],
            'code' => '123',
        ])->assertOk();

        $user->refresh();
        $this->assertEquals($user->password, $data->password);
        $this->assertNotNull($user->code);
    }
}
