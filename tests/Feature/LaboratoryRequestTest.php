<?php

namespace Tests\Feature;

use App\Models\Equipment;
use App\Models\Laboratory;
use App\Models\LaboratoryRequest;
use App\Models\Profile;
use App\Models\User;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Facades\Auth;

class LaboratoryRequestTest extends EquipmentTest
{
    protected Generator $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    protected function createLaboratoryRequest($attributes = [])
    {
        return LaboratoryRequest::factory()->state($attributes)->create();
    }

    public function testCreateRequest()
    {
        $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $data = [
            'equipment_id' => $equipment->id,
            'laboratory_id' => $laboratory->id,
            'description' => $this->faker->text(50),
            'date' => (new \DateTime())->modify('+ 10 day')->format('Y-m-d'),
        ];

        $response = $this
            ->post('/api/laboratory-requests', $data)
            ->assertJsonFragment([
                'user_id' => Auth::id(),
                'laboratory_id' => $laboratory->id,
                'equipment_id' => $equipment->id,
                'description' => $data['description'],
                'date' => $data['date'],
            ]);

        $response->assertCreated();
    }

    public function testCreateRequestWrongDate()
    {
        $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $data = [
            'equipment_id' => $equipment->id,
            'laboratory_id' => $laboratory->id,
            'description' => $this->faker->text(50),
            'date' => (new \DateTime())->modify('- 10 day')->format('Y-m-d'),
        ];

        $this
            ->post('/api/laboratory-requests', $data)
            ->assertUnprocessable();
    }

    public function testCreateRequestByUnauthorized()
    {
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $data = [
            'equipment_id' => $equipment->id,
            'laboratory_id' => $laboratory->id,
            'description' => $this->faker->text(50),
            'date' => (new \DateTime())->modify('+ 10 day')->format('Y-m-d'),
        ];

         $this
            ->post('/api/laboratory-requests', $data)
            ->assertUnauthorized();
    }

    public function testLaboratoryRequestList()
    {
        $user = $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $this->createLaboratoryRequest([
            'laboratory_id' => $laboratory->id,
            'equipment_id' => $equipment->id,
            'user_id' => $user->id,
        ]);

        $this->createUser(['role' => User::ROLE_ADMIN]);

        $response = $this
            ->get('/api/laboratory-requests')
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertArrayHasKey('id', $response['data'][0]);
        $this->assertArrayHasKey('description', $response['data'][0]);
        $this->assertArrayHasKey('date', $response['data'][0]);
        $this->assertArrayHasKey('laboratory', $response['data'][0]);
        $this->assertArrayHasKey('equipment', $response['data'][0]);
        $this->assertArrayHasKey('user', $response['data'][0]);
    }

    public function testLaboratoryRequestById()
    {
        $user = $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $laboratoryRequest = $this->createLaboratoryRequest([
            'laboratory_id' => $laboratory->id,
            'equipment_id' => $equipment->id,
            'user_id' => $user->id,
        ]);

        $this->createUser(['role' => User::ROLE_ADMIN]);

        $response = $this
            ->get("/api/laboratory-requests/$laboratoryRequest->id")
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertArrayHasKey('request', $response);
        $this->assertArrayHasKey('id', $response['request']);
        $this->assertArrayHasKey('description', $response['request']);
        $this->assertArrayHasKey('date', $response['request']);
        $this->assertArrayHasKey('laboratory', $response['request']);
        $this->assertArrayHasKey('equipment', $response['request']);
        $this->assertArrayHasKey('user', $response['request']);

        $this->assertArrayHasKey('laboratory', $response);
    }

    public function testLaboratoryRequestByWrongId()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);

        $response = $this
            ->get("/api/laboratory-requests/0")
            ->assertNotFound();
    }
}
