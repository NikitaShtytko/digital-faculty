<?php

namespace Tests\Feature;

use App\Models\Equipment;
use App\Models\Laboratory;
use App\Models\User;
use Faker\Generator;

class EquipmentTest extends LaboratoryTest
{
    protected Generator $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function createEquipment($attributes = [])
    {
        return Equipment::factory()->state($attributes)->create();
    }

    public function testCreateEquipment()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        $data = [
            'laboratory_id' => $laboratory->id,
            'description' => $this->faker->text(50),
        ];

        $response = $this
            ->post("/api/equipments", $data)
            ->assertJsonFragment([
                'description' => $data['description'],
                'photos' => [],
            ])
            ->assertCreated();

        $response = $response->decodeResponseJson();
        $this->assertEquals($response['data']['laboratory']['id'], $laboratory->id);
    }

    public function testCreateEquipmentByUser()
    {
        $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        $data = [
            'laboratory_id' => $laboratory->id,
            'description' => $this->faker->text(50),
        ];

        $this
            ->post("/api/equipments", $data)
            ->assertForbidden();
    }

    public function testUpdateEquipment()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $data = [
            'laboratory_id' => $laboratory->id,
            'description' => $this->faker->text(50),
        ];

        $response = $this
            ->put("/api/equipments/$equipment->id", $data)
            ->assertJsonFragment([
                'description' => $data['description'],
                'photos' => [],
            ])
            ->assertOk();

        $response = $response->decodeResponseJson();
        $this->assertEquals($response['data']['laboratory']['id'], $laboratory->id);
    }

    public function testUpdateEquipmentByUser()
    {
        $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $data = [
            'laboratory_id' => $laboratory->id,
            'description' => $this->faker->text(50),
        ];

        $this
            ->put("/api/equipments/$equipment->id", $data)
            ->assertForbidden();
    }

    public function testUpdateEquipmentWrongId()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);

        $this
            ->put("/api/equipments/0", [])
            ->assertNotFound();
    }

    public function testDeleteEquipment()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $this
            ->delete("/api/equipments/$equipment->id")
            ->assertOk();

        $this->assertNull(Equipment::query()->find($equipment->id));
    }

    public function testDeleteEquipmentByUser()
    {
        $this->createUser();
        /** @var Laboratory $laboratory */
        $laboratory = $this->createLaboratory();
        /** @var Equipment $equipment */
        $equipment = $this->createEquipment(['laboratory_id' => $laboratory->id]);

        $this
            ->delete("/api/equipments/$equipment->id")
            ->assertForbidden();
    }

    public function testDeleteEquipmentByWrongId()
    {
        $this->createUser(['role' => User::ROLE_ADMIN]);
        $this
            ->delete("/api/equipments/0")
            ->assertOk();
    }
}
